// Copyright (c) 2004-2023 Tomáš Oberhuber et al.
//
// This file is part of TNL - Template Numerical Library (https://tnl-project.org/)
//
// SPDX-License-Identifier: MIT

#pragma once

#include <TNL/Meshes/Topologies/SubentityVertexMap.h>
#include <TNL/Meshes/Topologies/Vertex.h>

namespace TNL::Meshes::Topologies {

struct Edge
{
   static constexpr int dimension = 1;
};

template<>
struct Subtopology< Edge, 0 >
{
   using Topology = Vertex;

   static constexpr int count = 2;
};

}  // namespace TNL::Meshes::Topologies
